FROM node

RUN apt-get update && apt-get install -y nano vim

RUN mkdir /skillbox
COPY package.json /skillbox
WORKDIR /skillbox
RUN yarn install

COPY . /skillbox

WORKDIR /skillbox

run yarn test
run yarn build

CMD yarn start

EXPOSE 3000